#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

#bf_upload_diagnostics(dry_run=False, contact_info='dylan.hamel@outlook.com')

from pybatfish.client.commands import *
from pybatfish.question.question import load_questions
from pybatfish.question import bfq

load_questions()
bf_session.host = "127.0.0.1"

#bf_init_snapshot('example/live')
bf_init_snapshot('cumulus_network/nclu')

print(f"{bfq.fileParseStatus().answer().frame()}")

ip_owners_ans = bfq.ipOwners().answer()
print(f"[ip_owners_ans] - \n{ip_owners_ans.frame()}")
